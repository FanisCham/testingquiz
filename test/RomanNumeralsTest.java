import org.junit.Before;

import java.awt.image.SampleModel;

import static org.junit.Assert.*;

public class RomanNumeralsTest {

    @org.junit.Test
    public void toRoman() {
        int num=5;
        String expectedOutput = "V";
        String res = RomanNumerals.toRoman(num);
        assertEquals(expectedOutput,res);
    }
    @org.junit.Test
    public void toRomanFailure() {
        int num=12;
        String expectedOutput = "null";
        String res = RomanNumerals.toRoman(num);
        assertEquals(expectedOutput,res);
    }
}